package com.example.francky.perpignancity;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.location.LocationListener;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;


public class Map extends FragmentActivity {
    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    private Location mLocationClient;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        this.setUpMapIfNeeded();

        //Button street view
        Button btnClickMe = (Button) findViewById(R.id.viewChange);
        btnClickMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){

                //Launch activity StreetView
                Intent intent = new Intent(Map.this,StreetView.class);
                startActivity(intent);

            }
        });
        /**/
    }
    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }
    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap();
            }
        }
    }
    private void setUpMap() {
        //Création d'un marker
        LatLng placeRepublique = new LatLng(42.698611, 2.895556);
        MarkerOptions markerPlaceRepublique = (new MarkerOptions().position(placeRepublique).icon(BitmapDescriptorFactory.fromResource(R.drawable.monument))).title("place de la république");
        mMap.addMarker(markerPlaceRepublique);
        //Listener pour capturer un click sur un marker et rendre le bouton visible
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(final Marker marker) {
                Button buttonGoToPosition = (Button) findViewById(R.id.gotoposition);
                buttonGoToPosition.setVisibility(View.VISIBLE);
                buttonGoToPosition.setText("Aller à " + marker.getTitle());

                //Ajoute des effets de style sur le bouton
                buttonGoToPosition.setTransformationMethod(null);
                buttonGoToPosition.setTextSize(15);

                //Listener pour rediriger l'utilisateur sur la navigation map google
                buttonGoToPosition.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view){
                        //Pour obtenir la position de l'utilisateur
                        LocationManager locationManager=    (LocationManager)getSystemService(Context.LOCATION_SERVICE);
                        String locationProvider = LocationManager.NETWORK_PROVIDER;// Or use LocationManager.GPS_PROVIDER
                        Location lastKnownLocation = locationManager.getLastKnownLocation(locationProvider);

                        //Lance une la map google avec le chemin pour aller à cet endroit via les bus
                        Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?saddr="+lastKnownLocation.getLatitude()+","+lastKnownLocation.getLongitude()+"&daddr="+marker.getPosition().latitude+","+marker.getPosition().longitude));
                        startActivity(intent);
                    }
                });
                return false;
            }
        });
        //Listener pour capturer un click sur la carte et cacher le bouton
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                Button buttonGoToPosition = (Button) findViewById(R.id.gotoposition);
                buttonGoToPosition.setVisibility(View.INVISIBLE);
            }
        });
        mMap.setMyLocationEnabled(true);
        //On désactive la mapToolbar
        mMap.getUiSettings().setMapToolbarEnabled(false);
        //Pour listner la changeement de position mais déprécié :
        //setOnMyLocationChangeListener(GoogleMap.OnMyLocationChangeListener listener)

    }

}