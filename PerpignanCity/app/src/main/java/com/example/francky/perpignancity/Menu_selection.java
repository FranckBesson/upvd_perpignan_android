package com.example.francky.perpignancity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * Created by Itazake on 04/05/2017.
 *
 * This is the 1st Activity launch by the application
 * It give a choice between different ways to use the app.
 *   for the moment (05/05/2017) there is :
 *     .Franck = Carousel with pictures of monuments.
 *                ...
 *     .Clement = List of 'type'/'categories' of location from the list containt in the app
 *                ...
 *
 */

public class Menu_selection extends Activity {

    private Button btnClick_StreetView = null;
    private Button btnClick_ListChoix = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_start);

        // 2 Buttons for select 2 differents types of usages
        // Each of them start a new activity
        btnClick_StreetView = (Button) findViewById(R.id.buton1);
        btnClick_ListChoix = (Button) findViewById(R.id.buton2);
        btnClick_StreetView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                //Launch activity StreetView
                Intent intent = new Intent(Menu_selection.this,Map.class);
                startActivity(intent);
            }
        });
        btnClick_ListChoix.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                //Launch activity ListChoix
                Intent intent = new Intent(Menu_selection.this,ListChoix.class);
                startActivity(intent);
            }
        });

    }
    @Override
    protected void onResume() {
        super.onResume();
    }
}
