package com.androidexperiments.landmarker;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.androidexperiments.landmarker.data.NearbyPlace;
import com.google.android.gms.nearby.Nearby;
import com.google.creativelabs.androidexperiments.typecompass.R;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import se.walkercrou.places.Place;

/**
 * Created by Francky on 07/06/2017.
 */

public class NavigationActivity extends BaseActivity {

    private NearbyPlace nearbyPlace;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);
        //Récupération de du type de recherche
        //placename = this.getIntent().getExtras().getString("place");
        nearbyPlace = (NearbyPlace) getIntent().getSerializableExtra("nearbyPlace");

        TextView textView = (TextView)findViewById(R.id.navigation_text);
        textView.setText(nearbyPlace.getName());
    }

}
