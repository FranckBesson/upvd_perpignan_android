package com.androidexperiments.landmarker;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.creativelabs.androidexperiments.typecompass.R;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import org.w3c.dom.Text;

/**
 * Splash shit
 */
public class SplashActivity extends BaseActivity
{
    private TextView btnClick_ListChoix = null;


    private static final String TAG = SplashActivity.class.getSimpleName();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_splash);

        btnClick_ListChoix = (TextView) findViewById(R.id.btn_selection);
        btnClick_ListChoix.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                //Launch activity ListChoix
                Intent intent = new Intent(SplashActivity.this, SelectionActivity.class);
                startActivity(intent);
            }
        });
        ButterKnife.inject(this);

    }

    @OnClick(R.id.btn_begin)
    public void onBeginClick()
    {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("placeType","");
        startActivity(intent);

        this.finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
