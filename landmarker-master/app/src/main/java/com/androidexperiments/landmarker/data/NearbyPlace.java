package com.androidexperiments.landmarker.data;

import android.location.Location;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.location.places.GeoDataApi;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import se.walkercrou.places.Place;

/**
 * Nearby place with the name (used for maps and display) and the
 * distance to that place, calculated against our current {@link Location}
 *
 * @see com.androidexperiments.landmarker.widget.DirectionalTextViewContainer#updatePlaces(List, Location)
 */
public class NearbyPlace
{
    private float distance;
    private String name;
    private Place place;

    public NearbyPlace(float distance, String name) {
        this.distance = distance;
        this.name = name;
    }

    public float getDistance() {
        return distance;
    }

    public String getPhotoReference(){
        String photoReference = "";

        try {
            photoReference = place.getJson().getJSONArray("photos").getJSONObject(0).getString("photo_reference");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return photoReference;
    }

    public String getName() {
        return name;
    }

    public void setPlace(Place place){
        this.place = place;
    }

    public Place getPlace(){
        return this.place;
    }

}
