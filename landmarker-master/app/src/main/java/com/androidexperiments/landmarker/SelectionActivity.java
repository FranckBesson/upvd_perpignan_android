package com.androidexperiments.landmarker;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.app.Activity;
import android.content.Intent;
import android.widget.RadioButton;
import android.widget.TextView;

import com.google.creativelabs.androidexperiments.typecompass.R;


public class SelectionActivity extends Activity {

    private RadioButton WC = null;
    private Button btnClick_ConfirmChoix = null;

    // list of all RadioButtons availables for the selection
    private RadioButton cb_atm = null;
    private RadioButton cb_bakery = null;
    private RadioButton cb_bank = null;
    private RadioButton cb_bar = null;
    private RadioButton cb_book_store = null;
    private RadioButton cb_bus_station = null;
    private RadioButton cb_cafe = null;
    private RadioButton cb_car_wash = null;
    private RadioButton cb_cemetery = null;
    private RadioButton cb_church = null;
    private RadioButton cb_clothing_store = null;
    private RadioButton cb_convenience_store = null;
    private RadioButton cb_courthouse = null;
    private RadioButton cb_dentist = null;
    private RadioButton cb_department_store = null;
    private RadioButton cb_doctor = null;
    private RadioButton cb_embassy = null;
    private RadioButton cb_fire_station = null;
    private RadioButton cb_florist = null;
    private RadioButton cb_gas_station = null;
    private RadioButton cb_gym = null;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selection);

        btnClick_ConfirmChoix = (Button) findViewById(R.id.confirm);
        cb_atm = (RadioButton) findViewById(R.id.atm);
        cb_bakery = (RadioButton) findViewById(R.id.bakery);
        cb_bank = (RadioButton) findViewById(R.id.bank);
        cb_bar = (RadioButton) findViewById(R.id.bar);
        cb_book_store = (RadioButton) findViewById(R.id.book_store);
        cb_bus_station = (RadioButton) findViewById(R.id.bus_station);
        cb_cafe = (RadioButton) findViewById(R.id.cafe);
        cb_car_wash = (RadioButton) findViewById(R.id.car_wash);
        cb_cemetery = (RadioButton) findViewById(R.id.cemetery);
        cb_church = (RadioButton) findViewById(R.id.church);
        cb_clothing_store = (RadioButton) findViewById(R.id.clothing_store);
        cb_convenience_store = (RadioButton) findViewById(R.id.convenience_store);
        cb_courthouse = (RadioButton) findViewById(R.id.courthouse);
        cb_dentist = (RadioButton) findViewById(R.id.dentist);
        cb_department_store = (RadioButton) findViewById(R.id.department_store);
        cb_doctor = (RadioButton) findViewById(R.id.doctor);
        cb_embassy = (RadioButton) findViewById(R.id.embassy);
        cb_fire_station = (RadioButton) findViewById(R.id.fire_station);
        cb_florist = (RadioButton) findViewById(R.id.florist);
        cb_gas_station = (RadioButton) findViewById(R.id.gas_station);
        cb_gym = (RadioButton) findViewById(R.id.gym);

        btnClick_ConfirmChoix.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                //Launch activity ListChoix
                Intent intent = new Intent(SelectionActivity.this, MainActivity.class);
                String placeTypeInfo = "";
                if(cb_atm.isChecked()){placeTypeInfo+="atm,";}
                if(cb_bakery.isChecked()){placeTypeInfo+="bakery,";}
                if(cb_bank.isChecked()){placeTypeInfo+="bank,";}
                if(cb_bar.isChecked()){placeTypeInfo+="bar,";}
                if(cb_book_store.isChecked()){placeTypeInfo+="book_store,";}
                if(cb_bus_station.isChecked()){placeTypeInfo+="station,";}
                if(cb_cafe.isChecked()){placeTypeInfo+="cafe,";}
                if(cb_car_wash.isChecked()){placeTypeInfo+="wash,";}
                if(cb_cemetery.isChecked()){placeTypeInfo+="cemetery,";}
                if(cb_church.isChecked()){placeTypeInfo+="church,";}
                if(cb_clothing_store.isChecked()){placeTypeInfo+="clothing_store,";}
                if(cb_convenience_store.isChecked()){placeTypeInfo+="convenience_store,";}
                if(cb_courthouse.isChecked()){placeTypeInfo+="courthouse,";}
                if(cb_dentist.isChecked()){placeTypeInfo+="dentist,";}
                if(cb_department_store.isChecked()){placeTypeInfo+="department_store,";}
                if(cb_doctor.isChecked()){placeTypeInfo+="doctor,";}
                if(cb_embassy.isChecked()){placeTypeInfo+="embassy,";}
                if(cb_fire_station.isChecked()){placeTypeInfo+="fire_station,";}
                if(cb_florist.isChecked()){placeTypeInfo+="florist,";}
                if(cb_gas_station.isChecked()){placeTypeInfo+="gas_station,";}
                if(cb_gym.isChecked()){placeTypeInfo+="gym,";}



                if (placeTypeInfo != null && placeTypeInfo.length() > 0 &&
                    placeTypeInfo.charAt(placeTypeInfo.length() - 1) == ',')
                {
                    placeTypeInfo = placeTypeInfo.substring(0, placeTypeInfo.length() - 1);
                }

                intent.putExtra("placeType",placeTypeInfo);
                startActivity(intent);
            }
        });

    }
    @Override
    protected void onResume() {
        super.onResume();
    }
}
